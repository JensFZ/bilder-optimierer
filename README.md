# Bilder Optimierer

Der Bilder Optimierer ist eine GUI für die [JPEG-Archive](https://github.com/danielgtaylor/jpeg-archive) Toolsammlung.

Copyright
---------
IF-Com GmbH 2018

Lizenz 
------
* Bilder Optimierer is copyright © 2018 IF-Com GmbH
* JPEG-Archive is copyright &copy; 2015 Daniel G. Taylor
* Image Quality Assessment (IQA) is copyright 2011, Tom Distler (http://tdistler.com)
* SmallFry is copyright 2014, Derek Buitenhuis (https://github.com/dwbuiten)

All are released under an MIT license.

http://dgt.mit-license.org/