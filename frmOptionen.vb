﻿Public Class frmOptionen
    Private Sub frmOptionen_Load(sender As Object, e As EventArgs) Handles Me.Load
        chkTimeStamp.Checked = My.Settings.TimeStamp
        txtTarget.Text = My.Settings.Target
        cboQuality.Text = My.Settings.Quality
        txtMinQuality.Text = My.Settings.MinQuality
        txtMaxQuality.Text = My.Settings.MaxQuality
        txtLoops.Text = My.Settings.Loops
        chkAccuracy.Checked = My.Settings.Accurate
        cboMethod.Text = My.Settings.Method
        chkStrip.Checked = My.Settings.Strip
    End Sub

    Private Sub btnAbbruch_Click(sender As Object, e As EventArgs) Handles btnAbbruch.Click
        Close()
    End Sub

    Private Sub btnOK_Click(sender As Object, e As EventArgs) Handles btnOK.Click

        My.Settings.TimeStamp = chkTimeStamp.Checked
        My.Settings.Target = txtTarget.Text
        My.Settings.Quality = cboQuality.Text
        My.Settings.MinQuality = txtMinQuality.Text
        My.Settings.MaxQuality = txtMaxQuality.Text
        My.Settings.Loops = txtLoops.Text
        My.Settings.Accurate = chkAccuracy.Checked
        My.Settings.Method = cboMethod.Text
        My.Settings.Strip = chkStrip.Checked

        Close()
    End Sub
End Class