﻿Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("Bilder Optimierer")>
<Assembly: AssemblyDescription("Der Bilder Optimierer ist eine GUI für die JPEG-Archive Utilities um JPEG Dateien in Ihrer Dateigröße zu reduzieren, ohne dass es ein für das Menschliche Auge erkennbaren Verlust an Informationen gibt.

JPEG-Archive is copyright © 2015 Daniel G. Taylor

Image Quality Assessment (IQA) is copyright 2011, Tom Distler (http://tdistler.com)

SmallFry is copyright 2014, Derek Buitenhuis (https://github.com/dwbuiten)")>
<Assembly: AssemblyCompany("IF-Com GmbH")>
<Assembly: AssemblyProduct("Bilder Optimierer")>
<Assembly: AssemblyCopyright("Copyright ©  2018")>
<Assembly: AssemblyTrademark("")>

<Assembly: ComVisible(False)>

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("ac3b2f4b-17fb-4f87-a62e-2f04339ccfd8")>

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers 
' by using the '*' as shown below:
' <Assembly: AssemblyVersion("1.0.*")> 

<Assembly: AssemblyVersion("1.0.*")>
<Assembly: AssemblyFileVersion("1.0.*")>
