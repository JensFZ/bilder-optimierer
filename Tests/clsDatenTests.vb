﻿Imports NUnit.Framework

<TestFixture>
Public Class clsDatenTests
    Const JPEG_TEST_FILE As String = "Test.jpg"

    <SetUp>
    Public Sub Setup()
        Const URL As String = "http://89.0.0.204/Jenkins/Test.jpg"
        If IO.File.Exists(JPEG_TEST_FILE) Then
            IO.File.Delete(JPEG_TEST_FILE)
        End If

        My.Computer.Network.DownloadFile(URL, JPEG_TEST_FILE)
        Console.WriteLine("test")
    End Sub

    <Test>
    Public Sub JPEG_TEST()

        If IO.File.Exists(JPEG_TEST_FILE) Then
            Dim OriginalFile As New IO.FileInfo(JPEG_TEST_FILE)
            Dim SmallFile As IO.FileInfo
            Dim tempObject As New clsDaten
            tempObject.File = JPEG_TEST_FILE

            'tempObject.Compress()

            SmallFile = New IO.FileInfo(JPEG_TEST_FILE)
            'Assert.That(OriginalFile.Length, [Is].GreaterThan(SmallFile.Length))
            Assert.That(OriginalFile.LastAccessTime, [Is].EqualTo(SmallFile.LastAccessTime))
            Assert.That(OriginalFile.LastWriteTime, [Is].EqualTo(SmallFile.LastWriteTime))
            Assert.That(OriginalFile.CreationTime, [Is].EqualTo(SmallFile.CreationTime))
        End If
    End Sub

    <TearDown>
    Public Sub TearDown()
        If IO.File.Exists(JPEG_TEST_FILE) Then
            IO.File.Delete(JPEG_TEST_FILE)
        End If
    End Sub
End Class