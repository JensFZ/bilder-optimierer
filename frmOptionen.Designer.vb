﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmOptionen
    Inherits System.Windows.Forms.Form

    'Das Formular überschreibt den Löschvorgang, um die Komponentenliste zu bereinigen.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Wird vom Windows Form-Designer benötigt.
    Private components As System.ComponentModel.IContainer

    'Hinweis: Die folgende Prozedur ist für den Windows Form-Designer erforderlich.
    'Das Bearbeiten ist mit dem Windows Form-Designer möglich.  
    'Das Bearbeiten mit dem Code-Editor ist nicht möglich.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.chkTimeStamp = New System.Windows.Forms.CheckBox()
        Me.txtTarget = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.cboQuality = New System.Windows.Forms.ComboBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtMinQuality = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txtMaxQuality = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txtLoops = New System.Windows.Forms.TextBox()
        Me.chkAccuracy = New System.Windows.Forms.CheckBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.cboMethod = New System.Windows.Forms.ComboBox()
        Me.chkStrip = New System.Windows.Forms.CheckBox()
        Me.btnOK = New System.Windows.Forms.Button()
        Me.btnAbbruch = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'chkTimeStamp
        '
        Me.chkTimeStamp.AutoSize = True
        Me.chkTimeStamp.Location = New System.Drawing.Point(12, 12)
        Me.chkTimeStamp.Name = "chkTimeStamp"
        Me.chkTimeStamp.Size = New System.Drawing.Size(189, 17)
        Me.chkTimeStamp.TabIndex = 0
        Me.chkTimeStamp.Text = "Zeitstempel der Bilder übernehemn"
        Me.chkTimeStamp.UseVisualStyleBackColor = True
        '
        'txtTarget
        '
        Me.txtTarget.Location = New System.Drawing.Point(12, 35)
        Me.txtTarget.Name = "txtTarget"
        Me.txtTarget.Size = New System.Drawing.Size(42, 20)
        Me.txtTarget.TabIndex = 1
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(60, 38)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(73, 13)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "Target Quality"
        '
        'cboQuality
        '
        Me.cboQuality.FormattingEnabled = True
        Me.cboQuality.Items.AddRange(New Object() {"low", "medium", "high", "veryhigh"})
        Me.cboQuality.Location = New System.Drawing.Point(12, 61)
        Me.cboQuality.Name = "cboQuality"
        Me.cboQuality.Size = New System.Drawing.Size(78, 21)
        Me.cboQuality.TabIndex = 3
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(96, 64)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(39, 13)
        Me.Label2.TabIndex = 4
        Me.Label2.Text = "Quality"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(60, 91)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(59, 13)
        Me.Label3.TabIndex = 6
        Me.Label3.Text = "Min Quality"
        '
        'txtMinQuality
        '
        Me.txtMinQuality.Location = New System.Drawing.Point(12, 88)
        Me.txtMinQuality.Name = "txtMinQuality"
        Me.txtMinQuality.Size = New System.Drawing.Size(42, 20)
        Me.txtMinQuality.TabIndex = 5
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(60, 117)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(62, 13)
        Me.Label4.TabIndex = 8
        Me.Label4.Text = "Max Quality"
        '
        'txtMaxQuality
        '
        Me.txtMaxQuality.Location = New System.Drawing.Point(12, 114)
        Me.txtMaxQuality.Name = "txtMaxQuality"
        Me.txtMaxQuality.Size = New System.Drawing.Size(42, 20)
        Me.txtMaxQuality.TabIndex = 7
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(60, 143)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(36, 13)
        Me.Label5.TabIndex = 10
        Me.Label5.Text = "Loops"
        '
        'txtLoops
        '
        Me.txtLoops.Location = New System.Drawing.Point(12, 140)
        Me.txtLoops.Name = "txtLoops"
        Me.txtLoops.Size = New System.Drawing.Size(42, 20)
        Me.txtLoops.TabIndex = 9
        '
        'chkAccuracy
        '
        Me.chkAccuracy.AutoSize = True
        Me.chkAccuracy.Location = New System.Drawing.Point(12, 166)
        Me.chkAccuracy.Name = "chkAccuracy"
        Me.chkAccuracy.Size = New System.Drawing.Size(153, 17)
        Me.chkAccuracy.TabIndex = 11
        Me.chkAccuracy.Text = "favor accuracy over speed"
        Me.chkAccuracy.UseVisualStyleBackColor = True
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(96, 192)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(39, 13)
        Me.Label6.TabIndex = 13
        Me.Label6.Text = "Quality"
        '
        'cboMethod
        '
        Me.cboMethod.FormattingEnabled = True
        Me.cboMethod.Items.AddRange(New Object() {"mpe", "ssim", "ms-ssim", "smallfry"})
        Me.cboMethod.Location = New System.Drawing.Point(12, 189)
        Me.cboMethod.Name = "cboMethod"
        Me.cboMethod.Size = New System.Drawing.Size(78, 21)
        Me.cboMethod.TabIndex = 12
        '
        'chkStrip
        '
        Me.chkStrip.AutoSize = True
        Me.chkStrip.Location = New System.Drawing.Point(12, 216)
        Me.chkStrip.Name = "chkStrip"
        Me.chkStrip.Size = New System.Drawing.Size(94, 17)
        Me.chkStrip.TabIndex = 14
        Me.chkStrip.Text = "Strip metadata"
        Me.chkStrip.UseVisualStyleBackColor = True
        '
        'btnOK
        '
        Me.btnOK.Location = New System.Drawing.Point(52, 239)
        Me.btnOK.Name = "btnOK"
        Me.btnOK.Size = New System.Drawing.Size(87, 51)
        Me.btnOK.TabIndex = 15
        Me.btnOK.Text = "OK"
        Me.btnOK.UseVisualStyleBackColor = True
        '
        'btnAbbruch
        '
        Me.btnAbbruch.Location = New System.Drawing.Point(145, 239)
        Me.btnAbbruch.Name = "btnAbbruch"
        Me.btnAbbruch.Size = New System.Drawing.Size(87, 51)
        Me.btnAbbruch.TabIndex = 16
        Me.btnAbbruch.Text = "Abbruch"
        Me.btnAbbruch.UseVisualStyleBackColor = True
        '
        'frmOptionen
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(284, 302)
        Me.Controls.Add(Me.btnAbbruch)
        Me.Controls.Add(Me.btnOK)
        Me.Controls.Add(Me.chkStrip)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.cboMethod)
        Me.Controls.Add(Me.chkAccuracy)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.txtLoops)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.txtMaxQuality)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.txtMinQuality)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.cboQuality)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.txtTarget)
        Me.Controls.Add(Me.chkTimeStamp)
        Me.Name = "frmOptionen"
        Me.Text = "Optionen"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents chkTimeStamp As CheckBox
    Friend WithEvents txtTarget As TextBox
    Friend WithEvents Label1 As Label
    Friend WithEvents cboQuality As ComboBox
    Friend WithEvents Label2 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents txtMinQuality As TextBox
    Friend WithEvents Label4 As Label
    Friend WithEvents txtMaxQuality As TextBox
    Friend WithEvents Label5 As Label
    Friend WithEvents txtLoops As TextBox
    Friend WithEvents chkAccuracy As CheckBox
    Friend WithEvents Label6 As Label
    Friend WithEvents cboMethod As ComboBox
    Friend WithEvents chkStrip As CheckBox
    Friend WithEvents btnOK As Button
    Friend WithEvents btnAbbruch As Button
End Class
