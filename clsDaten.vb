﻿Imports System.IO

Public Class clsDaten
    Public Property File As String
    Public Property LogError As String
    Public Property LogOutput As String
    Public Property State As eState

    Public Enum eState
        ToDo = 0
        Running = 1
        Completed = 2
    End Enum

    Private Sub DebugLog(Text As String)
        IO.File.AppendAllText("log.log", String.Format("{0:yyyy-MM-dd HH:mm:ss}: {1}" & Environment.NewLine, Now, Text))
    End Sub


    Public Function Compress() As Boolean
        _State = 1

        If IO.File.Exists(_File) Then
            Dim pInfo As New ProcessStartInfo()
            Dim dtCreate As DateTime = IO.File.GetCreationTime(_File)
            Dim dtLast As DateTime = IO.File.GetLastAccessTime(_File)
            Dim dtWrite As DateTime = IO.File.GetLastWriteTime(_File)

            Dim tempFile As String = IO.Path.GetTempFileName
            Console.WriteLine(Application.StartupPath)

            Select Case Path.GetExtension(_File).ToLower
                Case ".jpg", ".jpeg"
                    pInfo.FileName = Path.Combine(Application.StartupPath, "jpeg-archive", "jpeg-recompress.exe")
                    pInfo.Arguments = """" & _File & """ """ & tempFile & """"
                    pInfo.WindowStyle = ProcessWindowStyle.Hidden
                    pInfo.RedirectStandardError = True
                    pInfo.RedirectStandardInput = True
                    pInfo.RedirectStandardOutput = True
                    pInfo.UseShellExecute = False
                    pInfo.CreateNoWindow = True
                Case ".png"
                    pInfo.FileName = Path.Combine(Application.StartupPath, "Optipng", "optipng.exe")
                    pInfo.Arguments = """" & _File & """"
                    pInfo.WindowStyle = ProcessWindowStyle.Hidden
                    pInfo.RedirectStandardError = True
                    pInfo.RedirectStandardInput = True
                    pInfo.RedirectStandardOutput = True
                    pInfo.UseShellExecute = False
                    pInfo.CreateNoWindow = True
            End Select

            Dim p As Process = Process.Start(pInfo)
            p.WaitForExit()

            If IO.File.Exists(tempFile) Then
                If IO.File.Exists(_File) Then
                    IO.File.Delete(_File)
                End If
                IO.File.Move(tempFile, _File)
            End If

            _LogError = p.StandardError.ReadToEnd
            _LogOutput = p.StandardOutput.ReadToEnd

            Try
                IO.File.SetCreationTime(_File, dtCreate)
            Catch ex As Exception

            End Try

            Try
                IO.File.SetLastAccessTime(_File, dtLast)
            Catch ex As Exception

            End Try

            Try
                IO.File.SetLastWriteTime(_File, dtWrite)
            Catch ex As Exception

            End Try
        End If
        _State = 2
        Return True
    End Function
End Class