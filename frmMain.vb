﻿Imports System.Collections.ObjectModel
Public Class frmMain
    'Const PROGRESS_MAX = 1000
    ReadOnly lstDaten As New List(Of clsDaten)

    Private Sub Fortschritt(i As Integer)
        lstFortschritt.Text = String.Format("{0} / {1}", i, lstDaten.Count)
    End Sub

    Private Async Sub btnStart_Click(sender As Object, e As EventArgs) Handles btnStart.Click
        Dim progress As New Progress(Of Integer)(Sub(percent)
                                                     lstQue.Refresh()
                                                     ProgressBar1.Value = percent
                                                     Fortschritt(percent)
                                                 End Sub)

        btnStart.Enabled = False
        lstQue.Enabled = False
        AllowDrop = False
        ProgressBar1.Maximum = lstDaten.Count
        ProgressBar1.Step = 1
        ProgressBar1.Value = 0
        Await Task.Run(Sub()
                           Starte_Verarbeitung(progress)
                       End Sub)

        AllowDrop = True
        lstQue.Enabled = True
        btnStart.Enabled = True

        MessageBox.Show("Fertig")
        lstDaten.Clear()
        lstQue.Items.Clear()
    End Sub

    Private Sub addData(sDaten As String)
        Dim temp As New clsDaten
        temp.File = sDaten
        lstDaten.Add(temp)
        lstQue.Items.Add(sDaten)
        Fortschritt(0)
    End Sub

    Private Sub Form1_DragDrop(sender As Object, e As DragEventArgs) Handles Me.DragDrop
        Dim files() As String = e.Data.GetData(DataFormats.FileDrop)
        add_Entry(files)
    End Sub

    Private Sub add_Entry(Files() As String)
        For i As Integer = 0 To Files.Count - 1
            Dim path As String = Files(i)
            If IO.File.Exists(path) Then
                Check_File(path)
            ElseIf IO.Directory.Exists(path) Then
                Dim oFiles As ReadOnlyCollection(Of String)
                Dim filetypes As New List(Of String)
                filetypes.Add("*.jpg")
                filetypes.Add("*.jpeg")
                filetypes.Add("*.png")
                oFiles = My.Computer.FileSystem.GetFiles(path, FileIO.SearchOption.SearchAllSubDirectories, filetypes.ToArray)

                ProgressBar1.Maximum = oFiles.Count - 1
                ProgressBar1.Value = 0
                ProgressBar1.Step = 1
                For Each file In oFiles
                    Check_File(file)
                    ProgressBar1.PerformStep()
                Next
            End If
        Next
    End Sub

    Private Sub Check_File(path As String)
        If IO.Path.GetExtension(path).ToLower.EndsWith("jpg") Or IO.Path.GetExtension(path).ToLower.EndsWith("jpeg") Or IO.Path.GetExtension(path).ToLower.EndsWith("png") Then
            addData(path)
        End If
    End Sub

    Private Sub Form1_DragEnter(sender As Object, e As DragEventArgs) Handles Me.DragEnter
        If e.Data.GetDataPresent(DataFormats.FileDrop) Then
            e.Effect = DragDropEffects.Copy
        End If
    End Sub

    Private Sub Starte_Verarbeitung(progress As IProgress(Of Integer))
        Dim i As Integer

        Dim options As New ParallelOptions
        options.MaxDegreeOfParallelism = Environment.ProcessorCount


        Parallel.ForEach(lstDaten, options, Sub(oData As clsDaten)
                                                progress.Report(i)
                                                oData.Compress()
                                                i += 1
                                                progress.Report(i)
                                            End Sub)
    End Sub

    Private Sub lstQue_DrawItem(sender As Object, e As DrawItemEventArgs) Handles lstQue.DrawItem
        Dim index As Integer = e.Index
        If index >= 0 Then
            Dim g As Graphics = e.Graphics

            Dim oFont As Font = lstQue.Font
            'Dim color As Color = lstQue.ForeColor
            Dim sText As String = lstQue.Items(index).ToString

            e.DrawBackground()
            Select Case lstDaten(index).State
                Case clsDaten.eState.Running
                    g.FillRectangle(New SolidBrush(Color.Yellow), e.Bounds)
                Case clsDaten.eState.Completed
                    g.FillRectangle(New SolidBrush(Color.LightGreen), e.Bounds)
            End Select

            g.DrawString(sText, oFont, New SolidBrush(Color.Black), e.Bounds.X, e.Bounds.Y)
            e.DrawFocusRectangle()
        End If
    End Sub

    Private Sub btnAdd_Click(sender As Object, e As EventArgs) Handles btnAdd.Click
        OpenFileDialog1.FileName = ""
        If OpenFileDialog1.ShowDialog = DialogResult.OK Then
            add_Entry(OpenFileDialog1.FileNames)
        End If
    End Sub

    Private Sub btnInfo_Click(sender As Object, e As EventArgs) Handles btnInfo.Click
        Dim temp As New AboutBox1
        temp.ShowDialog()
    End Sub

    Private Sub btnListeLeeren_Click(sender As Object, e As EventArgs) Handles btnListeLeeren.Click
        lstDaten.Clear()
        lstQue.Items.Clear()
    End Sub

    Private Sub btnOptionen_Click(sender As Object, e As EventArgs) Handles btnOptionen.Click
        Dim formOptionen As New frmOptionen
        formOptionen.ShowDialog()
    End Sub

    Private Sub frmMain_Load(sender As Object, e As EventArgs) Handles Me.Load
        Text = String.Format("{0} {1} - Cores: {2}", My.Application.Info.ProductName, My.Application.Info.Version.ToString, Environment.ProcessorCount)
        Fortschritt(0)
    End Sub
End Class
