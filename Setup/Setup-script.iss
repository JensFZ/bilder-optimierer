[Setup]
VersionInfoVersion=1.0
VersionInfoCompany=IF-Com GmbH
VersionInfoDescription=Der Bilder Optimierer ist eine GUI f�r die JPEG-Archive Utilities um JPEG Dateien in Ihrer Dateigr��e zu reduzieren, ohne dass es ein f�r das Menschliche Auge erkennbaren Verlust an Informationen gibt.
VersionInfoTextVersion=1.0
VersionInfoCopyright=2018
VersionInfoProductName=Bilder Optimierer
VersionInfoProductVersion=1.0
AppCopyright=2018
AppName=Bilder Optimierer
AppVerName=1.0
DefaultDirName={pf}\IF-Com GmbH\Bilder Optimierer\
DefaultGroupName=IF-Com GmbH\Bilder Optimierer
OutputDir=D:\Projekte\Bilder Optimierer\Setup
ShowLanguageDialog=no
AppID={{325A3826-D572-483B-AA56-E97AE7A5C573}
OutputBaseFilename=setup_Bilder_Optimierer

[Files]
Source: ..\bin\Release\Bilder_Optimierer.exe; DestDir: {app}; Flags: replacesameversion
Source: ..\bin\Release\Bilder_Optimierer.exe.config; DestDir: {app}; Flags: replacesameversion
Source: ..\bin\Release\Bilder_Optimierer.pdb; DestDir: {app}; Flags: replacesameversion
Source: ..\bin\Release\Bilder_Optimierer.vshost.exe; DestDir: {app}; Flags: replacesameversion
Source: ..\bin\Release\Bilder_Optimierer.vshost.exe.config; DestDir: {app}; Flags: replacesameversion
Source: ..\bin\Release\Bilder_Optimierer.vshost.exe.manifest; DestDir: {app}; Flags: replacesameversion
Source: ..\bin\Release\Bilder_Optimierer.xml; DestDir: {app}; Flags: replacesameversion
Source: ..\bin\Release\jpeg-archive\jpeg-compare.exe; DestDir: {app}\jpeg-archive\; Flags: replacesameversion
Source: ..\bin\Release\jpeg-archive\jpeg-hash.exe; DestDir: {app}\jpeg-archive\; Flags: replacesameversion
Source: ..\bin\Release\jpeg-archive\jpeg-recompress.exe; DestDir: {app}\jpeg-archive\; Flags: replacesameversion
Source: ..\bin\Release\optipng\optipng.exe; DestDir: {app}\optipng\; Flags: replacesameversion
[Dirs]
Name: {app}\jpeg-archive
[Icons]
Name: {group}\Bilder Optimierer; Filename: {app}\Bilder_Optimierer.exe; WorkingDir: {app}
Name: {userdesktop}\Bilder Optimierer; Filename: {app}\Bilder_Optimierer.exe; WorkingDir: {app}
