﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmMain
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.btnStart = New System.Windows.Forms.Button()
        Me.OpenFileDialog1 = New System.Windows.Forms.OpenFileDialog()
        Me.btnAdd = New System.Windows.Forms.Button()
        Me.lstQue = New System.Windows.Forms.ListBox()
        Me.ProgressBar1 = New System.Windows.Forms.ProgressBar()
        Me.btnInfo = New System.Windows.Forms.Button()
        Me.btnListeLeeren = New System.Windows.Forms.Button()
        Me.btnOptionen = New System.Windows.Forms.Button()
        Me.lstFortschritt = New System.Windows.Forms.Label()
        Me.ClsDatenBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        CType(Me.ClsDatenBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'btnStart
        '
        Me.btnStart.Location = New System.Drawing.Point(12, 12)
        Me.btnStart.Name = "btnStart"
        Me.btnStart.Size = New System.Drawing.Size(88, 40)
        Me.btnStart.TabIndex = 1
        Me.btnStart.Text = "Start"
        Me.btnStart.UseVisualStyleBackColor = True
        '
        'OpenFileDialog1
        '
        Me.OpenFileDialog1.DefaultExt = "*.*"
        Me.OpenFileDialog1.FileName = "OpenFileDialog1"
        Me.OpenFileDialog1.Filter = "Alle Dateien|*.*"
        Me.OpenFileDialog1.Multiselect = True
        '
        'btnAdd
        '
        Me.btnAdd.Location = New System.Drawing.Point(106, 12)
        Me.btnAdd.Name = "btnAdd"
        Me.btnAdd.Size = New System.Drawing.Size(101, 40)
        Me.btnAdd.TabIndex = 2
        Me.btnAdd.Text = "Hinzufügen"
        Me.btnAdd.UseVisualStyleBackColor = True
        '
        'lstQue
        '
        Me.lstQue.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lstQue.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed
        Me.lstQue.FormattingEnabled = True
        Me.lstQue.Location = New System.Drawing.Point(12, 58)
        Me.lstQue.Name = "lstQue"
        Me.lstQue.Size = New System.Drawing.Size(725, 407)
        Me.lstQue.TabIndex = 3
        '
        'ProgressBar1
        '
        Me.ProgressBar1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ProgressBar1.Location = New System.Drawing.Point(12, 470)
        Me.ProgressBar1.Name = "ProgressBar1"
        Me.ProgressBar1.Size = New System.Drawing.Size(722, 19)
        Me.ProgressBar1.Style = System.Windows.Forms.ProgressBarStyle.Continuous
        Me.ProgressBar1.TabIndex = 6
        '
        'btnInfo
        '
        Me.btnInfo.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnInfo.Location = New System.Drawing.Point(700, 12)
        Me.btnInfo.Name = "btnInfo"
        Me.btnInfo.Size = New System.Drawing.Size(37, 40)
        Me.btnInfo.TabIndex = 7
        Me.btnInfo.Text = "Info"
        Me.btnInfo.UseVisualStyleBackColor = True
        '
        'btnListeLeeren
        '
        Me.btnListeLeeren.Location = New System.Drawing.Point(213, 12)
        Me.btnListeLeeren.Name = "btnListeLeeren"
        Me.btnListeLeeren.Size = New System.Drawing.Size(88, 40)
        Me.btnListeLeeren.TabIndex = 8
        Me.btnListeLeeren.Text = "Liste Leeren"
        Me.btnListeLeeren.UseVisualStyleBackColor = True
        '
        'btnOptionen
        '
        Me.btnOptionen.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnOptionen.Location = New System.Drawing.Point(606, 12)
        Me.btnOptionen.Name = "btnOptionen"
        Me.btnOptionen.Size = New System.Drawing.Size(88, 40)
        Me.btnOptionen.TabIndex = 9
        Me.btnOptionen.Text = "Optionen"
        Me.btnOptionen.UseVisualStyleBackColor = True
        Me.btnOptionen.Visible = False
        '
        'lstFortschritt
        '
        Me.lstFortschritt.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lstFortschritt.Location = New System.Drawing.Point(12, 492)
        Me.lstFortschritt.Name = "lstFortschritt"
        Me.lstFortschritt.Size = New System.Drawing.Size(722, 27)
        Me.lstFortschritt.TabIndex = 10
        Me.lstFortschritt.Text = "test"
        Me.lstFortschritt.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'ClsDatenBindingSource
        '
        Me.ClsDatenBindingSource.DataSource = GetType(Bilder_Optimierer.clsDaten)
        '
        'frmMain
        '
        Me.AllowDrop = True
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(746, 528)
        Me.Controls.Add(Me.lstFortschritt)
        Me.Controls.Add(Me.btnOptionen)
        Me.Controls.Add(Me.btnListeLeeren)
        Me.Controls.Add(Me.btnInfo)
        Me.Controls.Add(Me.ProgressBar1)
        Me.Controls.Add(Me.lstQue)
        Me.Controls.Add(Me.btnAdd)
        Me.Controls.Add(Me.btnStart)
        Me.DoubleBuffered = True
        Me.Name = "frmMain"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Bilder Optimierer"
        CType(Me.ClsDatenBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btnAdd As Button
    Friend WithEvents btnStart As Button
    Friend WithEvents OpenFileDialog1 As OpenFileDialog
    Friend WithEvents ClsDatenBindingSource As BindingSource
    Friend WithEvents lstQue As ListBox
    Friend WithEvents ProgressBar1 As ProgressBar
    Friend WithEvents btnInfo As Button
    Friend WithEvents btnListeLeeren As Button
    Friend WithEvents btnOptionen As Button
    Friend WithEvents lstFortschritt As Label
End Class
